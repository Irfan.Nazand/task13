﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.Dtos.ActorDtos;
using Task13.Models;

namespace Task13.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActorController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public ActorController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        private bool ActorExists(int id)
        {
            return _context.Actors.Any(e => e.Id == id);
        }

        //GET: api/Actor
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Actor>>> GetActors()
        {
            return await _context.Actors.ToListAsync();
        }

        // GET: api/Actors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActorDto>> GetActor(int id)
        {
            var actor = await _context.Actors.FindAsync(id);

            if (actor == null)
            {
                return NotFound();
            }

            ActorDto actorDto = _mapper.Map<ActorDto>(actor);
            
            return actorDto;
        }

        // GET: api/Actors/5
        // Get all the movies Actor has played in
        [HttpGet("movie/{id}")]
        public async Task<ActionResult<List<ActorDto>>> GetActorMovies(int id)
        {
            var actor = await _context.Actors.Include(a => a.MovieCharacters)
                .ThenInclude(a => a.Movie)
                .Where(a => a.Id == id).ToListAsync();

            if (actor == null)
            {
                return NotFound();
            }

            List<ActorDto> actorDto = _mapper.Map<List<ActorDto>>(actor);

            return actorDto;
        }

        // GET: api/Actors/5
        // Get all the Characters Actor has played
        [HttpGet("character/{id}")]
        public async Task<ActionResult<List<ActorCharactersDto>>> GetActorCharacters(int id)
        {
            var actor = await _context.Actors.Include(a => a.MovieCharacters)
                .ThenInclude(a => a.Character)
                .Where(a => a.Id == id).ToListAsync();

            if (actor == null)
            {
                return NotFound();
            }

            List<ActorCharactersDto> actorDto = _mapper.Map<List<ActorCharactersDto>>(actor);

            return actorDto;
        }

        // PUT: api/Actors/5
        // To protect from overposting attacks, enable the specific properties
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActor(int id, Actor actor)
        {
            if (id != actor.Id)
            {
                return BadRequest();
            }

            _context.Entry(actor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Actors
        // To protect from overposting attacks, enable the specific properties
        [HttpPost]
        public async Task<ActionResult<Actor>> PostActor(Actor actor)
        {
            _context.Actors.Add(actor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetActor", new { id = actor.Id }, actor);
        }

        // DELETE: api/Actors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Actor>> DeleteActor(int id)
        {
            var actor = await _context.Actors.FindAsync(id);
            if (actor == null)
            {
                return NotFound();
            }

            _context.Actors.Remove(actor);
            await _context.SaveChangesAsync();

            return actor;
        }
    }
}
