﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.Models;

namespace Task13.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchiseController : ControllerBase
    {
        private readonly MovieDbContext _context;

        public FranchiseController(MovieDbContext context)
        {
            _context = context;
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }

        //GET: api/Franchise
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Franchise>>> GetFranchises()
        {
            return await _context.Franchises.ToListAsync();
        }

        // GET: api/Franchise/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Franchise>> GetFranchise(int id)
        {
            var actor = await _context.Franchises.FindAsync(id);

            if (actor == null)
            {
                return NotFound();
            }

            return Ok(actor);
        }

        // GET: api/Franchise/5
        // Get all Movies in a Franchise
        [HttpGet("FranchiseMovies/{id}")]
        public async Task<ActionResult<List<Franchise>>> GetFranchiseMovies(int id)
        {
            var franchise = await _context.Franchises.Include(a => a.Movies)
                .Where(a => a.Id == id).ToListAsync();

            if (franchise == null)
            {
                return NotFound();
            }

            return franchise;
        }

        // GET: api/Franchise/5
        // Get all Characters in a Franchise
        [HttpGet("FranchiseCharacters/{id}")]
        public async Task<ActionResult<List<Franchise>>> GetFranchiseCharacters(int id)
        {
            var franchise = await _context.Franchises.Include(a => a.Movies)
                .ThenInclude(a => a.MovieCharacters).ThenInclude(a => a.Character)
                .Where(a => a.Id == id).ToListAsync();

            if (franchise == null)
            {
                return NotFound();
            }

            return franchise;
        }

        // PUT: api/Franchise/5
        // To protect from overposting attacks, enable the specific properties
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, Franchise franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Franchise
        // To protect from overposting attacks, enable the specific properties
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetActor", new { id = franchise.Id }, franchise);
        }

        // DELETE: api/Franchise/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Franchise>> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return franchise;
        }

    }
}
