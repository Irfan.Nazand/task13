﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.Models;

namespace Task13.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieCharacterController : ControllerBase
    {
        private readonly MovieDbContext _context;

        public MovieCharacterController(MovieDbContext context)
        {
            _context = context;
        }

        private bool MovieCharacterExists(int id)
        {
            return _context.MovieCharacters.Any(e => e.CharacterId == id);
        }

        //GET: api/MovieCharacter
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieCharacter>>> GetMovieCharacters()
        {
            return await _context.MovieCharacters.ToListAsync();
        }

        // GET: api/MovieCharacter/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieCharacter>> GetMovieCharacter(int id)
        {
            var movieCharacter = await _context.MovieCharacters.FindAsync(id);

            if (movieCharacter == null)
            {
                return NotFound();
            }

            return Ok(movieCharacter);
        }

        // PUT: api/MovieCharacters/5
        // To protect from overposting attacks, enable the specific properties
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovieCharacter(int id, MovieCharacter movieCharacter)
        {
            if (id != movieCharacter.CharacterId)
            {
                return BadRequest();
            }

            _context.Entry(movieCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieCharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MovieCharacter
        // To protect from overposting attacks, enable the specific properties
        [HttpPost]
        public async Task<ActionResult<MovieCharacter>> PostMovie(MovieCharacter movieCharacter)
        {
            _context.MovieCharacters.Add(movieCharacter);
           try
           { 
                await _context.SaveChangesAsync();
           }
           catch (DbUpdateException)
           {
                if (MovieCharacterExists(movieCharacter.CharacterId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
           }
            
            return CreatedAtAction("GetMovie", new { id = movieCharacter.CharacterId }, movieCharacter);
        }

        // DELETE: api/MovieCharacter/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieCharacter>> DeleteMovieCharacter(int id)
        {
            var movieCharacter = await _context.MovieCharacters.FindAsync(id);
            if (movieCharacter == null)
            {
                return NotFound();
            }

            _context.MovieCharacters.Remove(movieCharacter);
            await _context.SaveChangesAsync();

            return movieCharacter;
        }
    }
}
