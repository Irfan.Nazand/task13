﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Task13.Models;

namespace Task13
{
    public class MovieDbContext : DbContext
    {
        //Using tables
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieCharacter> MovieCharacters { get; set; }

        public MovieDbContext(DbContextOptions<MovieDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MovieCharacter>().HasKey(cc => new { cc.MovieId, cc.CharacterId });

            modelBuilder.Entity<Actor>().HasData(new Actor() { Id = 1, FirstName = "John", LastName = "Johnson", Gender = "Male", Dob = new DateTime(1963, 2, 17), PlaceOfBirth = "USA", Biography = "Great Actor, likes movies", Picture = " :/" });
            modelBuilder.Entity<Actor>().HasData(new Actor() { Id = 2, FirstName = "Irfan", LastName = "Nazand", Gender = "Male", Dob = new DateTime(1990, 2, 17), PlaceOfBirth = "USA", Biography = "Great Actor", Picture = " :/" });


            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 1, MovieTitle = "Micheal Jordan: AutoBiography", Genre = "Documentary", ReleaseYear = "2019", Director = "Irfan Nazand", Picture = "No picture", Trailer = "Coming Soon" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 3, MovieTitle = "Cristiano Ronaldo: AutoBiography", Genre = "Documentary", ReleaseYear = "2020", Director = "Irfan Nazand", Picture = "No picture", Trailer = "Coming Soon" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 4, MovieTitle = "Lionel Messi: AutoBiography", Genre = "Documentary", ReleaseYear = "2015", Director = "Irfan Nazand", Picture = "No picture", Trailer = "Coming Soon" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 5, MovieTitle = "Robert Lewandowski: AutoBiography", Genre = "Documentary", ReleaseYear = "2011", Director = "Irfan Nazand", Picture = "No picture", Trailer = "Coming Soon" });
        }
    }
}
