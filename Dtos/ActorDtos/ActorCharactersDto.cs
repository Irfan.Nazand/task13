﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.Dtos.CharacterDtos;

namespace Task13.Dtos.ActorDtos
{
    public class ActorCharactersDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        // add characterDto when created
        public ICollection<CharactersDto> Characters { get; set; }
    }
}
