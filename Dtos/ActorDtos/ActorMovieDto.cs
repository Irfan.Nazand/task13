﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13.Dtos.ActorDtos
{
    public class ActorMovieDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
       
        public ICollection<ActorMoviesDto> Movies { get; set; }
        //Add Movie dto when created
    }
}
