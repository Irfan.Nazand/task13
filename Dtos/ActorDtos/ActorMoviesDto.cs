﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.Dtos.MovieDtos;

namespace Task13.Dtos.ActorDtos
{
    public class ActorMoviesDto
    {
        public int MovieId { get; set; }

        public MovieDto Movie { get; set; }
    }
}
