﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.Dtos.CharacterDtos;

namespace Task13.Dtos.ActorDtos
{
    public class CharactersDto
    {
        public int CharacterId { get; set; }

        public CharacterDto Character { get; set; }
    }
}
