﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.Dtos.ActorDtos;

namespace Task13.Dtos.CharacterDtos
{
    public class ActorsPlayedCharacterDto
    {
        public int ActorId { get; set; }

        public ActorDto Actor { get; set; }
    }
}
