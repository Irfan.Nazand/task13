﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.Dtos.ActorDtos;

namespace Task13.Dtos.CharacterDtos
{
    public class CharacterActorDto
    {
        public string FullName { get; set; }
        public string Alias { get; set; }

        public ICollection<ActorsPlayedCharacterDto> ActorsPlayedCharacterDtos { get; set; }
        //Collection Navigation Property
    }
}
