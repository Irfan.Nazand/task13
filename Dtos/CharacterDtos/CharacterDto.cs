﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13.Dtos.CharacterDtos
{
    public class CharacterDto
    {

        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }

    }
}
