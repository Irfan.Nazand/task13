﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13.Dtos.FranchiseDtos
{
    public class FranchiseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<FranchiseCharactersDto> FranchiseCharacters { get; set; }
        public ICollection<FranchiseMoviesDto> FranchiseMvoies { get; set; }

    }
}
