﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.Dtos.ActorDtos;
using Task13.Dtos.CharacterDtos;

namespace Task13.Dtos.MovieDtos
{
    public class CharactersActorsInMovieDto
    {
        public CharacterDto Character { get; set; }

        public ActorDto Actor { get; set; }
    }
}
