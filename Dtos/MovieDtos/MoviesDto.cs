﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13.Dtos.MovieDtos
{
    public class MoviesDto
    {
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public string ReleaseYear { get; set; }
        public string Director { get; set; }
        
        public ICollection<CharactersActorsInMovieDto> CharactersActors { get; set; }

    }
}
