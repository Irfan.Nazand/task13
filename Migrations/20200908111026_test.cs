﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Task13.Migrations
{
    public partial class test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Movie_Franchise_FranchiseId",
                table: "Movie");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieCharacter_Actor_ActorId",
                table: "MovieCharacter");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieCharacter_Character_CharacterId",
                table: "MovieCharacter");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieCharacter_Movie_MovieId",
                table: "MovieCharacter");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MovieCharacter",
                table: "MovieCharacter");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Movie",
                table: "Movie");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Franchise",
                table: "Franchise");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Character",
                table: "Character");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Actor",
                table: "Actor");

            migrationBuilder.RenameTable(
                name: "MovieCharacter",
                newName: "MovieCharacters");

            migrationBuilder.RenameTable(
                name: "Movie",
                newName: "Movies");

            migrationBuilder.RenameTable(
                name: "Franchise",
                newName: "Franchises");

            migrationBuilder.RenameTable(
                name: "Character",
                newName: "Characters");

            migrationBuilder.RenameTable(
                name: "Actor",
                newName: "Actors");

            migrationBuilder.RenameIndex(
                name: "IX_MovieCharacter_CharacterId",
                table: "MovieCharacters",
                newName: "IX_MovieCharacters_CharacterId");

            migrationBuilder.RenameIndex(
                name: "IX_MovieCharacter_ActorId",
                table: "MovieCharacters",
                newName: "IX_MovieCharacters_ActorId");

            migrationBuilder.RenameIndex(
                name: "IX_Movie_FranchiseId",
                table: "Movies",
                newName: "IX_Movies_FranchiseId");

            migrationBuilder.AddColumn<int>(
                name: "ActorId1",
                table: "MovieCharacters",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_MovieCharacters",
                table: "MovieCharacters",
                columns: new[] { "MovieId", "CharacterId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Movies",
                table: "Movies",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Franchises",
                table: "Franchises",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Characters",
                table: "Characters",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Actors",
                table: "Actors",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_ActorId1",
                table: "MovieCharacters",
                column: "ActorId1");

            migrationBuilder.AddForeignKey(
                name: "FK_MovieCharacters_Actors_ActorId",
                table: "MovieCharacters",
                column: "ActorId",
                principalTable: "Actors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieCharacters_Franchises_ActorId1",
                table: "MovieCharacters",
                column: "ActorId1",
                principalTable: "Franchises",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieCharacters_Characters_CharacterId",
                table: "MovieCharacters",
                column: "CharacterId",
                principalTable: "Characters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieCharacters_Movies_MovieId",
                table: "MovieCharacters",
                column: "MovieId",
                principalTable: "Movies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Movies_Franchises_FranchiseId",
                table: "Movies",
                column: "FranchiseId",
                principalTable: "Franchises",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MovieCharacters_Actors_ActorId",
                table: "MovieCharacters");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieCharacters_Franchises_ActorId1",
                table: "MovieCharacters");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieCharacters_Characters_CharacterId",
                table: "MovieCharacters");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieCharacters_Movies_MovieId",
                table: "MovieCharacters");

            migrationBuilder.DropForeignKey(
                name: "FK_Movies_Franchises_FranchiseId",
                table: "Movies");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Movies",
                table: "Movies");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MovieCharacters",
                table: "MovieCharacters");

            migrationBuilder.DropIndex(
                name: "IX_MovieCharacters_ActorId1",
                table: "MovieCharacters");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Franchises",
                table: "Franchises");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Characters",
                table: "Characters");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Actors",
                table: "Actors");

            migrationBuilder.DropColumn(
                name: "ActorId1",
                table: "MovieCharacters");

            migrationBuilder.RenameTable(
                name: "Movies",
                newName: "Movie");

            migrationBuilder.RenameTable(
                name: "MovieCharacters",
                newName: "MovieCharacter");

            migrationBuilder.RenameTable(
                name: "Franchises",
                newName: "Franchise");

            migrationBuilder.RenameTable(
                name: "Characters",
                newName: "Character");

            migrationBuilder.RenameTable(
                name: "Actors",
                newName: "Actor");

            migrationBuilder.RenameIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movie",
                newName: "IX_Movie_FranchiseId");

            migrationBuilder.RenameIndex(
                name: "IX_MovieCharacters_CharacterId",
                table: "MovieCharacter",
                newName: "IX_MovieCharacter_CharacterId");

            migrationBuilder.RenameIndex(
                name: "IX_MovieCharacters_ActorId",
                table: "MovieCharacter",
                newName: "IX_MovieCharacter_ActorId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Movie",
                table: "Movie",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MovieCharacter",
                table: "MovieCharacter",
                columns: new[] { "MovieId", "CharacterId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Franchise",
                table: "Franchise",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Character",
                table: "Character",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Actor",
                table: "Actor",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Movie_Franchise_FranchiseId",
                table: "Movie",
                column: "FranchiseId",
                principalTable: "Franchise",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieCharacter_Actor_ActorId",
                table: "MovieCharacter",
                column: "ActorId",
                principalTable: "Actor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieCharacter_Character_CharacterId",
                table: "MovieCharacter",
                column: "CharacterId",
                principalTable: "Character",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieCharacter_Movie_MovieId",
                table: "MovieCharacter",
                column: "MovieId",
                principalTable: "Movie",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
