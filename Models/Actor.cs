﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13.Models
{
    public class Actor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime Dob { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Biography { get; set; }
        public string Picture { get; set; }

        //Collection Navigation Property
        public ICollection<MovieCharacter> MovieCharacters { get; set; }
    }
}
