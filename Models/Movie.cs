﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13.Models
{
    public class Movie
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public string ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }

        //Navigation Properties
        public Franchise Franchise { get; set; }

        //Collection Navigation Properties
        public ICollection<MovieCharacter> MovieCharacters { get; set; }
    }
}
