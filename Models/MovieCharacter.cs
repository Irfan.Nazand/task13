﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13.Models
{
    public class MovieCharacter
    {
        //Forreign Keys
        public int MovieId { get; set; }
        public int CharacterId { get; set; }

        //Navigation Properties
        public Movie Movie { get; set; }
        public Character Character { get; set; }
        public Franchise Actor { get; set; }
        
    }
}
