﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.Dtos.ActorDtos;
using Task13.Dtos.CharacterDtos;
using Task13.Dtos.MovieDtos;
using Task13.Models;

namespace Task13.Profiles
{
    public class ActorProfile : Profile
    {
        public ActorProfile()
        {
            CreateMap<Actor, ActorMovieDto>().ForMember(a => a.Movies,
               opt => opt.MapFrom(a => a.MovieCharacters));

            CreateMap<Actor, ActorCharactersDto>().ForMember(a => a.Characters,
               opt => opt.MapFrom(a => a.MovieCharacters));

            CreateMap<MovieCharacter, ActorMoviesDto>().ForMember(a => a.Movie,
               opt => opt.MapFrom(a => a.Movie));

            CreateMap<MovieCharacter, CharactersDto>().ForMember(a => a.Character,
               opt => opt.MapFrom(a => a.Character));

            CreateMap<Movie, MovieDto>();
            CreateMap<Character, CharacterDto>();
        }
    }
}
